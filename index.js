// Copyright 2020 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

// dexter-heatmap was written by Dan Gordon.

"use strict";

// Parameters:
let params = {
    // Spreads are relative to canvas coordinates.
    spread: 20.0, // Characteristic radius around each data point for calculating value on grid.
    spreadAlpha: 200.0, // Characteristic radius around each data point for calculating alpha on grid.
    colorMap: [
        {x: 0.00, rgba: [0, 0, 255, 255]},
        {x: 0.01, rgba: [0, 0, 255, 255]},

        {x: 0.05, rgba: [0, 128, 255, 255]},
        {x: 0.32, rgba: [0, 128, 255, 255]},

        {x: 0.38, rgba: [0, 255, 255, 255]},
        {x: 0.47, rgba: [0, 255, 255, 255]},

        {x: 0.53, rgba: [128, 255, 128, 255]},
        {x: 0.62, rgba: [128, 255, 128, 255]},

        {x: 0.68, rgba: [255, 255, 0, 255]},
        {x: 0.95, rgba: [255, 255, 0, 255]},

        {x: 0.99, rgba: [255, 128, 0, 255]},
        {x: 1.0, rgba: [255, 128, 0, 255]},
    ]
}; // Determines the colormap.

const DexterHeatmap = (function(parentContainer) {
    console.log("Loaded DexterHeatmap.");

    // The grid looks like this:
    //
    // +   +   +   +
    //   .   .   .  
    // +   +   +   +
    //   .   .   .  
    // +   +   +   +
    //   .   .   .  
    // +   +   +   +
    //
    // + = vertices at edges of grid squares (0 .. n^2 - 1)
    // . = vertices at center of grid squares (n^2 .. n^2 + nm1^2 - 1)
    //
    // Triangles are formed by joining the center of each square to the corners.

    const n = 64; // n x n grid.
    const n2 = n * n; // Number of grid points.
    const nm1 = n - 1; // Number of grid intervals in each dimension.
    const nm12 = nm1 * nm1; // Number of grid squares.
    function gridVertexIdx(i, j) {
        return i * n + j;
    }
    function gridCenterIdx(i, j) {
        return n2 + i * nm1 + j;
    }

    const nVert = n2 + nm12; // A vertex for each grid point, plus one at the center of each square.
    const nTri = 4 * nm12; // Each grid square has four triangles.
    const nTriVert = 3 * nTri; // Each triangle has three vertices. 

    let xMin = -1;
    let xMax = 1;
    let yMin = -1;
    let yMax = 1;

    const nCols = 512; // Number of colors in colormap.
    
    let canvas;
    let gl;

    let vertices;
    let vertexBuffer;
    let vertexPositionAttribute;

    let triVertices;
    let triVertexBuffer;

    let texCoord;
    let texCoordBuffer;
    let texCoordAttribute;
    
    let alpha;
    let alphaBuffer;
    let alphaAttribute;

    let tex;

    let shaderProgram;

    let vertexShaderSrc = "\
precision lowp float;\
attribute vec3 aPosition;\
attribute float aTexCoord;\
attribute float aAlpha;\
varying float vTexCoord;\
varying float vAlpha;\
\
void main(void) {\
vTexCoord = aTexCoord;\
vAlpha = aAlpha;\
gl_Position = vec4(aPosition, 1.0);\
}\
    ";

    let fragmentShaderSrc = "\
precision lowp float;\
uniform sampler2D uTexture;\
varying float vTexCoord;\
varying float vAlpha;\
\
void main(void) {\
vec4 color = texture2D(uTexture, vec2(vTexCoord, 0.5));\
color[3] = vAlpha;\
gl_FragColor = color;\
}\
    ";

    function setParams(p) {
        Object.assign(params, p);
    }

    function init(parentContainer) {
        canvas = document.createElement("canvas");
        canvas.id = "dexter-heatmap";
        canvas.style.display = "block";
        canvas.style.width = "100%";
        canvas.style.height = "100%";
        canvas.textContent = "Your browser doesn't appear to support the <code>&lt;canvas&gt;</code> element."
        parentContainer.appendChild(canvas) ;

        initWebGL(canvas);      // Initialize the GL context

        // Only continue if WebGL is available and working

        if (gl) {
            gl.clearColor(1, 1, 1, 1); // Clear to white, fully opaque
            gl.clearDepth(1.0); // Clear everything

            initShaders();
            initBuffers();
            initTextures();
            setViewRectToCanvas();
        }
    }
    
    function setData(dat) {
        function disp(v1, v2) {
            return [v1[0] - v2[0], v1[1] - v2[1]];
        }
        
        function disp2(disp) {
            return Math.pow(disp[0], 2) + Math.pow(disp[1], 2);
        }
        
        function addTo(v1, v2) {
            v1[0] += v2[0];
            v1[1] += v2[1];
        }

        function weight(d, s)
        {
            const dds = [d[0] / s, d[1] / s];
            const l2 = dds[0] * dds[0] + dds[1] * dds[1];
            return 1.0 / (1.0 + l2); // Cauchy distribution.
        }

        function toClip(xy) {
            return [2 * (xy[0] - xMin) / (xMax - xMin) - 1, -2 * (xy[1] - yMin) / (yMax - yMin) + 1];
        }

        // Scale to clip coordinates:
        const spreadAlphaClip = params.spreadAlpha / (xMax - xMin);
        const spreadAlphaClip2 = Math.pow(spreadAlphaClip, 2);
        const spreadClip = params.spread / (xMax - xMin);
        for (let i = 0; i < dat.length; ++i) {
            dat[i][0] = toClip(dat[i][0]);
        }

        // Bin the data:
        const nBin = 16; // n x n grid.
        
        function binIdx(xy) {
            const iBin = Math.floor(nBin * 0.5 * (xy[0] + 1));
            const jBin = Math.floor(nBin * 0.5 * (xy[1] + 1));
            return [iBin, jBin, nBin * iBin + jBin];
        }

        let bins = [];
        let nVisible = 0;
        for (let i = 0; i < dat.length; ++i) {
            if (Math.abs(dat[i][0][0]) > 1 || Math.abs(dat[i][0][1]) > 1) continue; // Ignore if off viewport.
            ++nVisible;
            const iBin = binIdx(dat[i][0]);
            const bin = bins[iBin[2]];
            if (bin) {
                bin.dataPoints.push(dat[i]);
                addTo(bin.xy, dat[i][0]);
                bin.val += dat[i][1];
            } else {
                bins[iBin[2]] = {idx: iBin, xy: dat[i][0].slice(), val: dat[i][1], dataPoints: [dat[i]]};
            }
        }

        bins.forEach(function (bin) {
            const count = bin.dataPoints.length;
            bin.val /= count;
            bin.xy[0] /= count;
            bin.xy[1] /= count;
        });

        for (let i = 0; i < nVert; ++i) {
            const vertXy = vertexXy(i);

            let totWeight = 0.0;
            let closestDist2 = 1e6;

            let val = 0.0;

            const vertBinIdx = binIdx(vertXy); // TODO: store.
           
            bins.forEach(function (bin) {
                if (Math.abs(bin.idx[0] - vertBinIdx[0]) < 3 && Math.abs(bin.idx[1] - vertBinIdx[1]) < 3) {
                    // Neighbouring bin, use all points.
                    for (let j = 0; j < bin.dataPoints.length; ++j) {
                        const dat = bin.dataPoints[j];
                        const d = disp(vertXy, dat[0]);

                        const wt = weight(d, spreadClip);
                        totWeight += wt;
                        val += wt * dat[1];
                       
                        const d2 = disp2(d);
                        if (d2 < closestDist2) closestDist2 = d2;
                    }
                } else {
                    // Non-neighbouring bin, use average.
                    const d = disp(vertXy, bin.xy);
                    const count = bin.dataPoints.length;

                    const wt = count * weight(d, spreadClip);
                    totWeight += wt;
                    val += wt * bin.val;
                
                    const d2 = disp2(d);
                    if (d2 < closestDist2) closestDist2 = d2;
                }
            });

            if (totWeight > 0.0) {
                val /= totWeight;
            }
            texCoord[i] = val;

            alpha[i] = Math.exp(-closestDist2 / spreadAlphaClip2);
        }

        gl.bindBuffer(gl.ARRAY_BUFFER, texCoordBuffer);
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, new Float32Array(texCoord));
        
        gl.bindBuffer(gl.ARRAY_BUFFER, alphaBuffer);
        gl.bufferSubData(gl.ARRAY_BUFFER, 0, new Float32Array(alpha));
    };

    function setViewRect(xMin_, yMin_, xMax_, yMax_) {
        xMin = xMin_;
        yMin = yMin_;
        xMax = xMax_;
        yMax = yMax_;
    }

    function setViewRectToCanvas() {
        setViewRect(0, 0, canvas.offsetWidth, canvas.offsetHeight);
    }

    function setViewRectToClip() {
        setViewRect(-1, -1, 1, 1);
    }

    function testData(nMin, nMax) {
        function rand(min, max)
        {
            return min + Math.random() * (max - min);
        }

        const nDat = Math.floor(rand(nMin, nMax));
        const dat = [];
        for (let i = 0; i < nDat; ++i)
        {
            dat.push([[rand(xMin, xMax), rand(yMin, yMax)], rand(0.0, 1.0)]);
        }
        return dat;
    }

    function draw() {
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, triVertexBuffer);
        gl.drawElements(gl.TRIANGLES, nTriVert, gl.UNSIGNED_SHORT, 0);
    }

    function initWebGL() {
        gl = null;

        try {
            gl = canvas.getContext("webgl", {
                premultipliedAlpha: false
            });
        }
        catch(e) {
        }

        if (!gl) {
            alert("Unable to initialize WebGL. Your browser may not support it.");
        }
    }

    function initShaders() {
        const vertexShader = getShader(gl, vertexShaderSrc, gl.VERTEX_SHADER);
        const fragmentShader = getShader(gl, fragmentShaderSrc, gl.FRAGMENT_SHADER);

        shaderProgram = gl.createProgram();
        gl.attachShader(shaderProgram, vertexShader);
        gl.attachShader(shaderProgram, fragmentShader);
        gl.linkProgram(shaderProgram);

        if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
            alert("Unable to initialize the shader program.");
        }

        gl.useProgram(shaderProgram);

        vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aPosition");
        gl.enableVertexAttribArray(vertexPositionAttribute);

        texCoordAttribute = gl.getAttribLocation(shaderProgram, "aTexCoord");
        gl.enableVertexAttribArray(texCoordAttribute);
        
        alphaAttribute = gl.getAttribLocation(shaderProgram, "aAlpha");
        gl.enableVertexAttribArray(alphaAttribute);
    }

    function initBuffers() {
        initVertices();
        vertexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, vertexBuffer);
        gl.vertexAttribPointer(vertexPositionAttribute, 3, gl.FLOAT, false, 0, 0);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertices), gl.STATIC_DRAW);

        initTriangles();
        triVertexBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, triVertexBuffer);
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(triVertices), gl.STATIC_DRAW);

        texCoordBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, texCoordBuffer);
        gl.vertexAttribPointer(texCoordAttribute, 1, gl.FLOAT, false, 0, 0);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(texCoord), gl.DYNAMIC_DRAW);
        
        alphaBuffer = gl.createBuffer();
        gl.bindBuffer(gl.ARRAY_BUFFER, alphaBuffer);
        gl.vertexAttribPointer(alphaAttribute, 1, gl.FLOAT, false, 0, 0);
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(alpha), gl.DYNAMIC_DRAW);
    };

    function initTextures() {
        tex = gl.createTexture();
        let cols = [];
        for (let i = 0; i < nCols; ++i) {
            const x = (i / (nCols - 1));
            cols.push(colorAt(x))
        }
        cols = [].concat.apply([], cols); // Flatten.
        const oneDTextureTexels = new Uint8Array(cols);
        gl.bindTexture(gl.TEXTURE_2D, tex);
        gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, nCols, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, oneDTextureTexels);

        // gl.NEAREST is also allowed, instead of gl.LINEAR, as neither mipmap.
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);

        // Prevents s-coordinate wrapping (repeating).
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);

        // Prevents t-coordinate wrapping (repeating).
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

        gl.bindTexture(gl.TEXTURE_2D, null);

        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, tex);
        gl.uniform1i(gl.getUniformLocation(shaderProgram, "uTexture"), 0);

    }

    function getShader(gl, src, shaderType) {
        const shader = gl.createShader(shaderType);
        gl.shaderSource(shader, src);
        gl.compileShader(shader);
        if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
            alert("An error occurred compiling the shaders: " + gl.getShaderInfoLog(shader));
            return null;
        }
        return shader;
    }

    function initVertices() {
        function gridCoord(i) {
            return 2 * i / nm1 - 1;
        }

        function centerCoord(i) {
            return gridCoord(i + 0.5);
        }

        vertices = [];
        for (let i = 0; i < n; ++i) {
            const x = gridCoord(i);
            for (let j = 0; j < n; ++j) {
                const y = gridCoord(j);
                vertices.push.apply(vertices, [x, y, 0.0]);
            }
        }
        for (let i = 0; i < n - 1; ++i) {
            const x = centerCoord(i);
            for (let j = 0; j < n - 1; ++j) {
                const y = centerCoord(j);
                vertices.push.apply(vertices, [x, y, 0.0]);
            }
        }

        texCoord = [];
        alpha = [];
        for (let i = 0; i < nVert; ++i)
        {
            texCoord[i] = 0.0;
            alpha[i] = 0.0;
        }
    }

    function initTriangles() {

        triVertices = [];
        for (let i = 0; i < n - 1; ++i) {
            for (let j = 0; j < n - 1; ++j) {
                triVertices.push.apply(
                    triVertices,
                    [
                        gridCenterIdx(i, j), gridVertexIdx(i, j), gridVertexIdx(i, j + 1),
                        gridCenterIdx(i, j), gridVertexIdx(i, j + 1), gridVertexIdx(i + 1, j + 1),
                        gridCenterIdx(i, j), gridVertexIdx(i + 1, j + 1), gridVertexIdx(i + 1, j),
                        gridCenterIdx(i, j), gridVertexIdx(i + 1, j), gridVertexIdx(i, j)
                    ]
                );
            }
        }
    }

    function vertexXy(i) {
        return vertices.slice(3 * i, 3 * i + 2);
    }

    function colorAt(x) {
        let result;

        const colorMap = params.colorMap;

        if (x <= 0) {
            result = colorMap[0].rgba;
        } else if (x >= 1) {
            result = colorMap[colorMap.length - 1].rgba
        } else {
            let i1 = 0;
            for (i1 = 1; i1 < colorMap.length; ++i1)
            {
                if (colorMap[i1].x > x) break;
            }
            const i0 = i1 - 1;
            const x0 = colorMap[i0].x;
            const x1 = colorMap[i1].x;
            const fx0 = (x1 - x) / (x1 - x0);
            const fx1 = (x - x0) / (x1 - x0);

            const rgba0 = colorMap[i0].rgba;
            const rgba1 = colorMap[i1].rgba;
            result = [
                fx0 * rgba0[0] + fx1 * rgba1[0],
                fx0 * rgba0[1] + fx1 * rgba1[1],
                fx0 * rgba0[2] + fx1 * rgba1[2],
                fx0 * rgba0[3] + fx1 * rgba1[3]
            ];
        }
        return result;
    };

    return {
        setParams: setParams,
        init: init,
        setData: setData,
        setViewRect: setViewRect,
        setViewRectToCanvas: setViewRectToCanvas,
        setViewRectToClip: setViewRectToClip,
        draw: draw,
        testData: testData
    };
}());

const testDexterHeatmap = (function () {
  const heatmapCont = document.getElementById("heatmap-container");
  DexterHeatmap.init(heatmapCont);
  // DexterHeatmap.setViewRectToClip();
  setInterval(function() {
    DexterHeatmap.setData(DexterHeatmap.testData(20, 20));
    DexterHeatmap.draw();}, 500);
});

module.exports = {
    DexterHeatmap: DexterHeatmap,
    testDexterHeatmap: testDexterHeatmap
};
