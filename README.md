#dexter-heatmap - a WebGL-based heatmap

See [e-network-viewer](https://gitlab.com/SmartGridToolbox/e-network-viewer) for an example of use.

Other JavaScript/WebGL heatmaps assign colours based on the density of points - where more points will give "hotter" colours. This is typically done very efficiently based on a neat WebGL colour-blending trick, but is only useful when we are interested in point density. dexter-heatmap, on the other hand, assigns a given heat / colour to each specified point, and colours will then be assigned to the display based on the colours of the nearest points.
